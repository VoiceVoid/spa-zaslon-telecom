import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { User } from '../models/user';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  baseUrl = 'https://localhost:44358/api/user';

  constructor(private http: HttpClient) { }

  getUsers(searchTerm?: string): Observable<User[]> {
    let params = new HttpParams();
    if (!!searchTerm) {
      params = params.append('searchTerm', searchTerm);
    }
    return this.http.get<User[]>(this.baseUrl, {params});
  }

  createUser(user: User): Observable<User> {
    return this.http.post<User>(this.baseUrl, user);
  }
}
