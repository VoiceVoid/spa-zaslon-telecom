import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { User } from '../models/user';
import { Observable } from 'rxjs';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  usersList: User[];
  ifCreate = false;
  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.getUsers(null);

  }

  getUsers(searchTerm: string) {
    this.userService.getUsers(searchTerm).subscribe((users: User[]) => {
    this.usersList = users;
   });
  }

  onSubmit(userForm: NgForm) {
      this.userService.createUser(userForm.value).subscribe(user => {
        this.closeUser();
        this.getUsers(null);
      });
  }

  searchUser(searchTerm: string) {
    this.getUsers(searchTerm);
  }

  closeUser(){
    this.ifCreate = false;
  }

  showCreateUser(){
    this.ifCreate = !this.ifCreate;
  }


}
